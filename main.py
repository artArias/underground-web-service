# -*- coding: utf-8 -*-

import sys
import cgi
import os
import webapp2
from jinja2 import Environment, FileSystemLoader


# Список соответствия цвета веток и станций на этой ветке
colours = {
    u'желтую': [
        u'Улица дыбенко',
        u'Проспект большевиков',
        u'Ладожская',
        u'Новочеркасская',
        u'Площадь александра невского - 2',
        u'Лиговский проспект',
        u'Достоевская',
        u'Спасская'
    ],
    u'красную': [
        u'Девяткино',
        u'Гражданский проспект',
        u'Академическая',
        u'Политехническая',
        u'Площадь мужества',
        u'Лесная',
        u'Выборгская',
        u'Площадь ленина',
        u'Чернышевская',
        u'Площадь восстания',
        u'Владимирская',
        u'Пушкинская',
        u'Технологический институт - 1', 
        u'Балтийская',
        u'Нарвская',
        u'Кировский завод',
        u'Автово',
        u'Ленинский проспект',
        u'Проспект ветеранов'
    ],
    u'голубую': [
        u'Парнас',
        u'Проспект просвещения',
        u'Озерки',
        u'Удельная',
        u'Пионерская',
        u'Чёрная речка',
        u'Петроградская',
        u'Горьковская',
        u'Невский проспект',
        u'Сенная площадь',
        u'Технологический институт - 2',
        u'Фрунзенская',
        u'Московские ворота',
        u'Электросила',
        u'Парк победы',
        u'Московская',
        u'Звёздная',
        u'Купчино'
    ],
    u'зелёную': [
        u'Приморская',
        u'Василеостровская',
        u'Гостиный двор',
        u'Маяковская',
        u'Площадь александра невского - 1',
        u'Елизаровская',
        u'Ломоносовская',
        u'Пролетарская',
        u'Обухово',
        u'Рыбацкое'
    ],
    u'фиолетовую': [
        u'Комендантский проспект',
        u'Старая деревня',
        u'Крестовский остров',
        u'Чкаловская',
        u'Спортивная',
        u'Адмиралтейская',
        u'Садовая',
        u'Звенигородская',
        u'Обводный канал',
        u'Волковская',
        u'Бухарестская',
        u'Международная'
    ]
}

# Функции проверки цвета ветки, на которую переходим
def is_yellow(colour):
    if colour == u'желтую':
        return True


def is_red(colour):
    if colour == u'красную':
        return True


def is_blue(colour):
    if colour == u'голубую':
        return True


def is_green(colour):
    if colour == u'зелёную':
        return True


def is_purple(colour):
    if colour == u'фиолетовую':
        return True


# Функция вывода правильного окончания для времени в пути
def ending(time):
    if time >= 5 and time <=20:
        return u'минут'
    elif time == 1:
        return u'минуту'
    elif time >= 2 and time <= 4:
        return u'минуты'
    elif int(str(time)[-1]) == 1:
        return u'минуту'
    elif int(str(time)[-1]) >=2 and int(str(time)[-1]) <=4:
        return u'минуты'
    else:
        return u'минут'


# Функция вывода правильного окончания количества пересадок
def transition_ending(count):
    if count == 1:
        return u'пересадка'
    if count == 5:
        return u'пересадок'
    else:
        return u'пересадки'


# Функция проверки, была ли выбрана закрытая станция(Выборгская)
def is_closed(start, finish):
    if start == u'Выборгская' or finish == u'Выборгская':
        return True


# Функция, возвращающая правильное название для веток из двух и более слов, где не только первое слово пишется с большой буквы
def is_capital_letter(station):    
        if station == u'Площадь мужества':
            return u'Площадь Мужества'
        elif station == u'Площадь ленина':
            return u'Площадь Ленина'
        elif station == u'Площадь восстания':
            return u'Площадь Восстания'
        elif station == u'Проспект ветеранов':
            return u'Проспект Ветеранов'
        elif station == u'Парк победы':
            return u'Парк Победы'
        elif station == u'Проспект просвещения':
            return u'Проспект Просвещения'
        elif station == u'Старая деревня':
            return u'Старая Деревня'
        elif station == u'Проспект большевиков':
            return u'Проспект Большевиков'
        elif station == u'Улица дыбенко':
            return u'Улица Дыбенко'
        elif station == u'Площадь александра невского - 1':
            return u'Площадь Александра Невского - 1'
        elif station == u'Площадь александра невского - 2':
            return u'Площадь Александра Невского - 2'
        else:
            return station


# Объвление функций глобальными для использования их в html файле
jinja_env = Environment(loader=FileSystemLoader(os.path.dirname(__file__)))
jinja_env.globals['is_green'] = is_green
jinja_env.globals['is_red'] = is_red
jinja_env.globals['is_purple'] = is_purple
jinja_env.globals['is_yellow'] = is_yellow
jinja_env.globals['is_blue'] = is_blue
jinja_env.globals['ending'] = ending
jinja_env.globals['is_closed'] = is_closed
jinja_env.globals['is_capital_letter'] = is_capital_letter
jinja_env.globals['transition_ending'] = transition_ending


# Функция поиска наименее затратного пути от станции отправления до станции назначения
def shortestpath(graph, start, end, attempt, visited= [], distances = {}, parent = {}):
    if attempt == 0:
        visited = []
        distances = {}
        parent = {}
    if start == end:
        path = []
        while end != None:
            path.append(end)
            end = parent.get(end,None)
        return distances[start], path[::-1]
    if not visited:
        distances[start] = 0
    for neighbor in graph[start]:
        if neighbor not in visited:
            neighbor_dist = distances.get(neighbor,10 ** 6)
            test_dist = distances[start] + graph[start][neighbor]
            if test_dist < neighbor_dist:
                distances[neighbor] = test_dist
                parent[neighbor] = start
    visited.append(start)
    unvisited = dict((k, distances.get(k,10 ** 6)) for k in graph if k not in visited)
    nearest_node = min(unvisited, key=unvisited.get)
    attempt += 1
    return shortestpath(graph, nearest_node, end, attempt, visited, distances, parent)


# Граф метро Санкт - Петербурга
graph = {
    u'Приморская': {u'Василеостровская': 4},
    u'Василеостровская': {u'Гостиный двор': 4, u'Приморская': 4},
    u'Гостиный двор': {u'Маяковская': 3, u'Невский проспект': 2, u'Василеостровская': 4},
    u'Маяковская': {u'Гостиный двор': 3, u'Площадь александра невского - 1': 3, u'Площадь восстания': 2},
    u'Площадь александра невского - 1': {u'Маяковская': 3, u'Площадь александра невского - 2': 2, u'Елизаровская': 5},
    u'Елизаровская': {u'Площадь александра невского - 1': 5, u'Ломоносовская': 3},
    u'Ломоносовская': {u'Пролетарская': 3, u'Елизаровская': 3},
    u'Пролетарская': {u'Обухово': 3, u'Ломоносовская': 3},
    u'Обухово': {u'Пролетарская': 3, u'Рыбацкое': 4},
    u'Рыбацкое': {u'Обухово': 4},
    u'Невский проспект': {u'Гостиный двор': 2, u'Сенная площадь': 2, u'Горьковская': 4},
    u'Горьковская': {u'Невский проспект': 4, u'Петроградская': 2},
    u'Петроградская': {u'Горьковская': 2, u'Чёрная речка': 4},
    u'Чёрная речка': {u'Пионерская': 3, u'Петроградская': 4},
    u'Пионерская': {u'Удельная': 3, u'Чёрная речка': 3},
    u'Удельная': {u'Озерки': 3, u'Пионерская': 3},
    u'Озерки': {u'Проспект просвещения': 2, u'Удельная': 3},
    u'Проспект просвещения': {u'Озерки': 2, u'Парнас': 3},
    u'Парнас': {u'Проспект просвещения' : 3},
    u'Сенная площадь': {u'Спасская' : 3, u'Технологический институт - 2': 3, u'Невский проспект': 2, u'Садовая': 3},
    u'Технологический институт - 2': {u'Сенная площадь': 3, u'Технологический институт - 1': 1, u'Фрунзенская': 2},
    u'Фрунзенская': {u'Технологический институт - 2': 2, u'Московские ворота': 2},
    u'Московские ворота': {u'Фрунзенская': 2, u'Электросила': 2},
    u'Электросила': {u'Московские ворота': 2, u'Парк победы': 2},
    u'Парк победы': {u'Электросила': 2, u'Московская': 3},
    u'Московская': {u'Парк победы': 3, u'Звёздная': 4},
    u'Звёздная': {u'Московская': 4, u'Купчино': 3},
    u'Купчино': {u'Звёздная': 3},
    u'Садовая': {u'Сенная площадь': 3, u'Спасская': 3, u'Звенигородская': 4, u'Адмиралтейская': 3},
    u'Адмиралтейская': {u'Садовая': 3, u'Спортивная': 3},
    u'Спортивная': {u'Чкаловская': 2, u'Адмиралтейская': 3},
    u'Чкаловская': {u'Спортивная': 2, u'Крестовский остров': 4},
    u'Крестовский остров': {u'Чкаловская': 4, u'Старая деревня': 3},
    u'Старая деревня': {u'Комендантский проспект': 3, u'Крестовский остров': 3},
    u'Комендантский проспект': {u'Старая деревня': 3},
    u'Звенигородская': {u'Пушкинская': 2, u'Садовая': 4, u'Обводный канал': 3},
    u'Обводный канал': {u'Волковская': 3, u'Звенигородская': 3},
    u'Волковская': {u'Обводный канал': 3, u'Бухарестская': 3},
    u'Бухарестская': {u'Волковская': 3, u'Международная': 3},
    u'Международная': {u'Бухарестская': 3},
    u'Спасская': {u'Сенная площадь': 3, u'Садовая': 3, u'Достоевская': 4},
    u'Достоевская': {u'Спасская': 4, u'Владимирская': 2, u'Лиговский проспект': 2},
    u'Лиговский проспект': {u'Достоевская': 2, u'Площадь александра невского - 2': 2},
    u'Площадь александра невского - 2': {u'Площадь александра невского - 1': 2, u'Лиговский проспект': 2, u'Новочеркасская': 3},
    u'Новочеркасская': {u'Площадь александра невского - 2': 3, u'Ладожская': 3},
    u'Ладожская': {u'Новочеркасская': 3, u'Проспект большевиков': 3},
    u'Проспект большевиков': {u'Ладожская': 3, u'Улица дыбенко': 2},
    u'Улица дыбенко': {u'Проспект большевиков': 2},
    u'Пушкинская': {u'Звенигородская': 2, u'Технологический институт - 1': 2, u'Владимирская': 2},
    u'Владимирская': {u'Пушкинская': 2, u'Достоевская': 2, u'Площадь восстания': 2},
    u'Площадь восстания': {u'Владимирская': 2, u'Маяковская': 2, u'Чернышевская': 2},
    u'Чернышевская': {u'Площадь восстания': 2, u'Площадь ленина': 3},
    u'Площадь ленина': {u'Чернышевская': 3, u'Выборгская': 3},
    u'Выборгская': {u'Площадь ленина': 3, u'Лесная': 2},
    u'Лесная': {u'Выборгская': 2, u'Площадь мужества': 3},
    u'Площадь мужества': {u'Лесная': 3, u'Политехническая': 3},
    u'Политехническая': {u'Площадь мужества': 3, u'Академическая': 2},
    u'Академическая': {u'Политехническая': 2, u'Гражданский проспект': 6},
    u'Гражданский проспект': {u'Академическая': 6, u'Девяткино': 3},
    u'Девяткино': {u'Гражданский проспект': 3},
    u'Технологический институт - 1': {u'Пушкинская': 2, u'Технологический институт - 2': 1, u'Балтийская': 2},
    u'Балтийская': {u'Технологический институт - 1': 2, u'Нарвская': 3},
    u'Нарвская': {u'Кировский завод': 4, u'Балтийская': 3},
    u'Кировский завод': {u'Нарвская': 4, u'Автово': 2},
    u'Автово': {u'Кировский завод': 2, u'Ленинский проспект': 3},
    u'Ленинский проспект': {u'Проспект ветеранов': 2, u'Автово': 3},
    u'Проспект ветеранов': {u'Ленинский проспект': 2},
}


# Получаем время и маршрут из переменной result
def receive_answer(result):
    time = result[0]
    route = result[1]
    return time, route


# Функция расчета пересадок с одной ветки на другую
def transition_calculation(route, colours):
    transition = {}
    max_transition = 0
    for i in range(len(route) - 1):
        for j in colours:
            if route[i] in colours[j] and route[i + 1] in colours[j]:
                break
            else:
                if route[i + 1] in colours[j]:
                    new_dict = {route[i + 1]: j}
                    transition.update(new_dict)
    return transition


# Функция проверки наличия пересадок
def have_transition(transition):
    if len(transition) != 0:
        return True
    else:
        return False


# Отрисовка главной страницы
class MainPage(webapp2.RequestHandler):
    def get(self):
        template = jinja_env.get_template('underground.html')
        self.response.write(template.render())


# Обработка данных, полученных со стартовой страницы, передаем значения переменных для отрисовки на странице результата
class ResultPage(webapp2.RequestHandler):
    def post(self):
        station1 = self.request.get("station1").strip().capitalize()
        station2 = self.request.get("station2").strip().capitalize()
        try:
            time, route = receive_answer(shortestpath(graph, station1, station2, 0))
            transition = transition_calculation(route, colours)
            template_values = {
                'start': station1,
                'finish': station2,
                'time': time,
                'flag': have_transition(transition),
                'count': len(transition),
                'route': route,
                'transition': transition,
                'check': False
            }
        except:
            template_values = {
                'check': True
            }
        template = jinja_env.get_template('underground.html')
        self.response.write(template.render(template_values))

application = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/result', ResultPage),
], debug=True)


def main():
    application.run()


if __name__ == "__main__":
    main()
